<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class IndexController extends Controller
{
    public function index(Request $request)
    {
        $searchWord = $request->input('search');
        $bookModelQuery = DB::table('books as b')
            ->select('b.name as bookName', 'year', 'img', 'price',
                DB::raw("GROUP_CONCAT(a.name SEPARATOR ' , ') as name"))
            ->join('authors as a', function ($query) use ($searchWord) {
                $query->on('b.isbn', '=', 'a.isbn');
            })->groupBy('b.name', 'year', 'img', 'price');
        if ($request->filled('search')) {
            $bookModelQuery = $bookModelQuery->orwhereRaw('lower(a.name) like (?)', ["%{$searchWord}%"])
                ->orWhereRaw('lower(b.name) like (?)', ["%{$searchWord}%"]);
        }
        $bookModel = $bookModelQuery->paginate(10);

        return view('index', ['bookModels' => $bookModel]);
    }
}
