<?php

namespace App\Console\Commands;

use App\Jobs\FirstSiteJob;
use Illuminate\Console\Command;

class StartTask extends Command
{
    /**
     * Array site parsers
     */
    const LIST_SITE_FOR_PARSE = [
        'https://www.piter.com/collection/biblioteka-programmista' => FirstSiteJob::class,
        'https://www.piter.com/collection/kompyutery-i-internet' => FirstSiteJob::class,
        'https://www.piter.com/collection/nauka-i-obrazovanie' => FirstSiteJob::class,
        'https://www.piter.com/collection/dom-byt-dosug' => FirstSiteJob::class,
        'https://www.piter.com/collection/otraslevaya-ekonomika' => FirstSiteJob::class,
        'https://www.piter.com/collection/avtorskie-metodiki-istseleniya' => FirstSiteJob::class,
        'https://www.piter.com/collection/grafika-dizayn-cad' => FirstSiteJob::class,

    ];
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'task';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Start layout task';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        foreach (self::LIST_SITE_FOR_PARSE as $key => $value) {

            $a = new $value;
            $a->thisUrl = $key;
            dispatch($a);
        }
    }
}
