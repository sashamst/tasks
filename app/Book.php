<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Book
 * @package App
 *
 * Property
 * @property $isbn integer
 * @property $name string
 * @property $year integer
 * @property $img string
 * @property $price integer
 *
 * Relations
 * @property Author[] $authors
 *
 */
class Book extends Model
{
    public $fillable = ['isbn', 'name', 'year', 'img', 'price'];

    public function authors()
    {
        return $this->hasMany(Author::class, 'isbn', 'isbn');
    }
}
