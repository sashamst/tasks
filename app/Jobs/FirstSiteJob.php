<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Author;
use App\Book;
use Goutte\Client;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

class FirstSiteJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $thisUrl;

    /**
     * FirstSiteJob constructor.
     */
    public function __construct()
    {

    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $client = new Client();
        $crawler = $client->request('GET', $this->thisUrl);

        $crawler->filter('.book-block')->each(function (\Symfony\Component\DomCrawler\Crawler $node)
        use ($client) {
            $link = $node->filter('a')->link();
            $oneLink = $client->click($link);
            $bookIsbn = preg_replace('/ISBN:|-|\s+/', '', $oneLink->filter('.params li')->last()->text());
            $bookName = $oneLink->filter('.product-info h1')->text();
            $bookYear = preg_replace('/Год:|-|\s+/', '', $oneLink->filter('.params li')->getNode(1)->textContent);
            $bookAuthor = trim($oneLink->filter('.author')->getNode(0)->textContent);
            $bookPrice = explode(' ', $oneLink->filter('.price')->text())[0];
            $fileUrl = $oneLink->filter('.photo img')->image()->getUri();
            $info = pathinfo($fileUrl);
            $contents = file_get_contents($fileUrl);
            $file = '/tmp/' . $info['basename'];
            file_put_contents($file, $contents);
            $uploaded_file = new UploadedFile($file, $info['basename']);
            $bookFilePath = Storage::putFile('', $uploaded_file);

            DB::beginTransaction();
            try {
                $bookModel = Book::where(['isbn' => $bookIsbn])->first();
                if ( ! $bookModel) {
                    $bookModel = new Book();
                    $bookModel->isbn = $bookIsbn;
                    $bookModel->name = $bookName;
                    $bookModel->year = $bookYear;
                    $bookModel->img = $bookFilePath;
                    $bookModel->price = $bookPrice;
                    try {
                        if ( ! $bookModel->save()) {
                            throw new \Exception('Book model dont save');
                        } else {
                            Log::info('Save Book' . $bookIsbn . PHP_EOL);
                        }
                    } catch (\Exception $e) {
                        Log::warning($e->getMessage() . "Book model dont save" . PHP_EOL);
                    }
                    $author = explode(',', $bookAuthor);
                    $authors = $bookModel->authors();
                    if (count($author) === 1) {
                        try {
                            if ( ! $authors->save(new Author([
                                'isbn' => $bookIsbn,
                                'name' => $author[0]
                            ]))) {
                                throw new \Exception('Book model once author dont save' . PHP_EOL);
                            } else {
                                Log::info('Save one author' . $bookIsbn . ' - ' . $author[0] . PHP_EOL);
                            }
                        } catch (\Exception $e) {
                            Log::warning($e->getMessage() . 'Book model once author dont save' . PHP_EOL);
                        };
                    } else {
                        foreach ($author as $authorValue) {
                            try {
                                if ( ! $authors->save(new Author([
                                    'isbn' => $bookIsbn,
                                    'name' => $authorValue
                                ]))) {
                                    throw new \Exception('Book model poly-author dont save' . PHP_EOL);
                                } else {
                                    Log::info('Save one author' . $bookIsbn . ' - ' . $authorValue . PHP_EOL);
                                }
                            } catch (\Exception $e) {
                                Log::warning($e->getMessage());
                            }
                        }
                    }
                } else {
                    return;
                }
            } catch (\Exception $e) {
                Log::warning($e->getMessage() . 'indefinite error' . PHP_EOL);
                DB::rollBack();
            }
            DB::commit();
        });
    }
}
