<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Author
 * @package App
 *
 * Property
 * @property $id integer
 * @property $isbn integer
 * @property string $name
 */
class Author extends Model
{
    public $fillable = ['isbn', 'name'];
}
