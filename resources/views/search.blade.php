<div class="container form">
    <form>
        <div class="col-md-9">
            <div class="input-group">
                <input type="text" class="form-control" placeholder="Поиск по названию" name="search"
                       value="{{request('search')}}">
                <span class="input-group-btn"><button class="btn-info" type="submit">Найти</button></span>
            </div>
        </div>
    </form>
</div>