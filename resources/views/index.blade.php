@extends('layout.layout')
@section('content')
    @include('search')

    <div class="row border">
        <div class="col-2">Название</div>
        <div class="col-2">Год издания</div>
        <div class="col-4">Изображение</div>
        <div class="col-2">Авторы</div>
        <div class="col-2">Стоимость</div>
    </div>
    @foreach($bookModels as $book)
        <div class="row border">
            <div class="col-2">{{$book->bookName}}</div>
            <div class="col-2">{{$book->year}}</div>
            <div class="col-4"><img alt="{{$book->bookName}}" src="{{asset('storage/'.$book->img)}}"></div>
            <div class="col-2">{{$book->name}}</div>
            <div class="col-2">{{$book->price}}</div>
        </div>
    @endforeach
    {{ $bookModels->links() }}
@endsection

